# Design Information
1. A user will be able to choose to log in as a specific player or create a new player when starting the application. For simplicity, any authentication is optional, and you may assume there is a single system running the application.

I made two classes: *User* and *Player*. I put association between those two classes so that one *User* can have multiple *Player*.

2. The application will allow players to (1) create a cryptogram, (2) choose a cryptogram to solve, (3) solve cryptograms, (4) view their list of completed cryptograms, and (5) view the list of cryptogram statistics.

For the first step, I added *Cryptogram* class.

For the second and third step, I added *ChooseAndSolve* class.

For the fourth step, I first added Boolean attribute called *completed* inside *ChooseAndSolve* class to distinguish which cryptogram is completed. Then, I added the operation called *getCompletedCryptogram()* which should return the list of completed cryptograms inside *ChooseAndSolve* class.

For the fifth step, I added an operation called *getCryptogramList()* which should return all the cryptograms inside *ChooseAndSolve* class.

3. A cryptogram will have an encoded phrase (encoded with a simple substitution cipher), a solution, and a maximum number of allowed solution attempts. A cryptogram will only encode alphabetic characters, but may include other characters (such as punctuation or white space) in the puzzle, which will remain unaltered. Capitalization is preserved when encoded.

To realize this requirement, I added a class called *Cryptogram* with attributes such as String (*name*, *solution*, and *encoded*) and Integer (*maxAttempt*). For encoding operation, I added an operation called *pair* inside *Cryptogram* class.

4. To add a player, the user will enter the following player information:
- A first name

I added attribute for String called *firstName* inside *Player* class

- A last name

I added attribute for String called *lastName* inside *Player* class

- A unique username

I added attribute for String called *username* inside *Player* class. Then, I added an operation called *setUsername (String username)* which checks if *username* is unique. If it is unique, it will set *username* with the input. If not, it will print the message to notify that *username* is not unique.

- An email

I added an attribute for String called *email* inside *Player* class.

5. To add a new cryptogram, a player will:

First, the program should create a Cryptogram object called *tempCryptogram*. Then, user should enter the values on it.

- Enter a unique cryptogram puzzle name.

I added String type attribute called *puzzleName* and an operation called *setPuzzleName (String puzzleName)* inside *Cryptogram* class. 

- Enter a solution (unencoded) phrase.

I added String type attribute called *solution* and an operation called *setSolution (String solution)* inside *Cryptogram* class.

- Choose different letters to pair with every letter present in the phrase.

I added an operation called *pair* which takes the previous solution phrase as a parameter. It will return String. When the operation is called, it should read each letter in that phrase and prompt user to enter a letter which he wishes to pair with. After, all the letters in the phrase are paired, then it will return the converted phrase and store in String attribute called *encoded* in *Cryptogram* class. 

- View the encoded phrase.

As mentioned previously, *pair* operation will return the encoded phrase. Therefore, GUI can shows the String type attribute *encoded* to allow the user to view the encoded phrase. 

- Enter a maximum number of allowed solution attempts.

I added an operation called *setMaxAttempt* which takes Integer input and save it to the Integer type attribute called *maxAttempt* in *Cryptogram* class.
 
- Edit any of the above steps as necessary.

This is not implemented in my design. GUI should call the previous operations whenever user clicks appropriate button for the operation.

- Save the complete cryptogram.

I added an operation called *save (ArrayList <Cryptogram> cryptogramList, Cryptogram tempCryptogram)* which first check to if *tempCryptogram* has unique *puzzleName* which is not in *cryptogramList*.  If that *puzzleName* is unique, then it will append the *tempCryptogram* object at the end of *cryptogramList*.

- View a confirmation that the name assigned to the cryptogram is unique and return to the main menu.

As mentioned in the previous step, if *puzzleName* is unique, the *save* operation will append the object at the end of the *cryptogramList* and print confirmation message for GUI to show.

6. To choose and solve a cryptogram, a player will:

- Choose a cryptogram from a list of all unsolved cryptograms.

To realize this requirement, I first added *ChooseAndSolve* class. Then, I added an operation called *getUnsolvedCryptograms()* which returns the *ArrayList<Cryptogram>* that I created called *unsolvedCryptogramList*.

- View the chosen cryptogram and number of incorrect solution submissions (starting at 0 for a cryptogram that has not previously been attempted).

I created Integer type attribute called *incorrectAttempt* in *ChooseAndSolve* class to store the incorrect solution submission.

- Match the replacement and encrypted letters together, and view the resulting potential solution.

I added an operation called *match* that takes the String attribute *encoded* as a parameter. This operation will ask the replacement character for each character in *encoded*. It will finally return a String based on user input. I added a String type attribute called *matchResult* to take that return value.

- When all letters in the puzzle are replaced and they are satisfied with the potential solution, submit their answer.

I added an operation called *submit* that takes the String attribute as a parameter in this case *matchResult* from previous operation *match*. Inside the operation, it will compare to String attribute of the actual *Cryptogram* class which is called *solution*. The *submit* operation will return Boolean type attribute to show whether the attempt is matched to solution or not. If it is successful, then the Boolean attribute *solved* should become true. 

- Get a result indicating that the solution was successful or incrementing the number of incorrect solution submissions if it was unsuccessful.

As mentioned previously, *submit* operation will return Boolean value to see whether the solution is successful or not.  I created *resultAction* operation which takes the Boolean value from *submit* and Integer type attribute called *incorrectAttempt* to increase its value if the Boolean attribute *submit* is false. It should also print the result on the screen so that user can see.

- At any point, the player may return to the list of unsolved cryptograms to try another.

I added an operation called *getUnsolvedCryptogramList* to return ArrayList of *unsolvedCryptogramList*.

- If the number of incorrect solution attempts increases to the maximum allowed number of solution attempts, they will get a result that the cryptogram game was lost, and this cryptogram will be moved to the completed list.

I added an operation called *isMaximum* that takes the Integer attribute *incorrectAttempt* and another Integer attribute called *maxAttempt* from *Cryptogram* class. If *incorrectAttempt* is bigger than *maxAttempt*, *isMaximum* will return *true* and store that Boolean value to the Boolean attribute that I created called *completed*. Then, the system will call another operation called *setComplete* which should append the current cryptogram to the ArrayList<Cryptogram> called *completedCryptogramList*. Also, the system should call utility operation from *OperatingSystem* called *getDate* to recorded completion time. It should be stored in Date attribute called *completedDate*.


7. The list of unsolved cryptograms will show the cryptogram�s name and the number of incorrect solution attempts (if any) for that player.

I can use the operation called *getUnsolvedCryptogramList* that returns *ArrayList<Cryptogram> unsolvedCryptogramList*. The system should access each *Cryptogram*�s attributes such as *puzzleName* and *incorrectAttempt*, and print those so that user can see.

8. The list of completed cryptograms for that player will show the cryptogram�s name, whether it was successfully solved, and the date it was completed.

I added an operation called *getCompletedCryptogramList* that returns the ArrayList<Cryptogram> called *completedCryptogramList* that I created from previous step. The system can access to attributes such as *puzzleName*, *solved*, and *completedDate* to print their values so that user can see.  

9. The list of cryptogram statistics will display a list of cryptograms in order of creation. Choosing each cryptogram will display the date it was created, the number of players who have solved the cryptogram, and the username of the first three players to solve the cryptogram.

To realize this requirement, I created Date attribute called *createdDate*. Since after every creation of *Cryptogram*, it will append to the last element of the *ArrayList<Cryptogram> cryptogramList*, the list will be in order of creation. The system should have access to those attributes such as *createdDate*, *numPlayerSolved* which holds the Integer, and *firstThreePlayer* which is *ArrayList<Player>. Finally, the system can call the operation *getCryptogramList* to return *ArrayList<Cryptogram> cryptogramList*. Then, the system can print them so that user can see. 

10. The User Interface (UI) must be intuitive and responsive.

This is not represented in my design, as it will be handled entirely within the GUI implementation.

11. The performance of the game should be such that players does not experience any considerable lag between their actions and the response of the game.

This is not represented in my design. It should depend on data structure- such as Big O and server performance.


