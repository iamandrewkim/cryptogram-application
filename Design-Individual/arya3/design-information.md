# Design Information

## Requirements

1. A user will be able to choose to log in as a specific player or create a new player when starting the application.  For simplicity, any authentication is optional, and you may assume there is a single system running the application.

The 'Application' class that handles logic for the entire _game system_ has a method called *login* and has access to the database that has each player's username and password stored. The 'Player' class has a create method that the system can access if the user chooses to make a new player.

2. The application will allow players to  (1) create a cryptogram, (2) choose a cryptogram to solve, (3) solve cryptograms, (4) view their list of completed cryptograms, and (5) view the list of cryptogram statistics.

The 'Cryptogram' class has a method to create a cryptogram. The 'Application' class that handles _game system_ logic has methods to choose a cryptogram and start an instance of gameplay called 'Cryptogram Game'. The 'Cryptogram Game' class handles specific game logic for a player trying to solve a puzzle and has a method _submit_ to solve the cryptogram. The 'Application' class has methods to get the list of completed cryptograms for the player that's logged in by accessing the database that stores this information and storing it in the completedCryptograms attribute. The 'Application' class also has a method for getting the cryptogram statistic and an attibute cryptogramStatistics for storing it. completedCryptograms and cryptogramStatitics are 'Lists' and have their own classes: 'CompletedCryptogram' and 'CryptogramStatistics' which help with displaying the right info in the list view and ordering of the list.

3. A cryptogram will have an encoded phrase (encoded with a simple substitution cipher), a solution, and a maximum number of allowed solution attempts.  A cryptogram will only encode alphabetic characters, but may include other characters (such as punctuation or white space) in the puzzle, which will remain unaltered.  Capitalization is preserved when encoded.

The 'Cryptogram' class has attributes for the solution, the cipher and maximum number of allowed solutions called 'maxAttempts'. The encoded phrase is represented by the derived field '/phrase' that gets calcucalted using the solution and the cipher. The constraints mentioned for encoding are an implentation detail and will be handled later in the code.

4. To add a player, the user will enter the following player information:
	a. A first name
	b. A last name
	c. A unique username
	d. An email

The 'Player' class has attributes 'firstName', 'lastName', 'userName' (unique), and 'email' to account for this info. The uniqueness of the username is checked with the 'confirmUsernameIsUnique' method on the class that checks the database and makes sure this new username is unique.

5. To add a new cryptogram, a player will:
	a. Enter a unique cryptogram puzzle name.
	b. Enter a solution (unencoded) phrase.
	c. Choose different letters to pair with every letter present in the phrase.
	d. View the encoded phrase.
	e. Enter a maximum number of allowed solution attempts.
	f. Edit any of the above steps as necessary.
	g. Save the complete cryptogram.
	h. View a confirmation that the name assigned to the cryptogram is unique and return to the main menu.  

The GUI will have a buttom for creating a new cryptogram and this will then pop up a new GUI form which underneath will use the 'create' method on the 'Cryptogram' class. The method 'confirmNameIsUnique' on the 'Cryptogram' class will check the database and make sure the entered name is unique against already created cryptograms. The 'edit' and 'save' methods on the 'Cryptogram' class allow the player to edit the steps mentioned above and save the completed cryptogram. On a 'save', the GUI will show a confirmation and this logic will be implemented in the GUI code. The 'encode' method of the 'Cryptogram' class is what usees the 'cipher' on the 'solution' string and created the 'phrase'.

6. To choose and solve a cryptogram, a player will:
	a. Choose a cryptogram from a list of all unsolved cryptograms.
	b. View the chosen cryptogram and number of incorrect solution submissions (starting at 0 for a cryptogram that has not previously been attempted).
	c. Match the replacement and encrypted letters together, and view the resulting potential solution.
	d. When all letters in the puzzle are replaced and they are satisfied with the potential solution, submit their answer.
	e. Get a result indicating that the solution was successful, or incrementing the number of incorrect solution submissions if it was unsuccessful.
	f. At any point, the player may return to the list of unsolved cryptograms to try another.
	g. If the number of incorrect solution attempts increases to the maximum allowed number of solution attempts, they will get a result that the cryptogram game was lost, and this cryptogram will be moved to the completed list.

The 'Application' class has a method 'chooseCryptogram' that lets a player select an unsolved cryptogram and creates a game instance called 'Cryptogram Game'. 'Cryptogram Game' has attributes 'failedAttempts' for the number of failed attempts that defaults to 0, 'testCipher' for the current cipher the player is trying to test if correct, and '/potentialSolution' which is a derived attribute for the potential solution that comes from the test cipher and the 'decode' method on the 'Cryptogram' class. As the player matches replacement and encrypted letters, the 'testCipher' attribute changes and the '/potentialSolution' attriubute updates as well. When a player submits, the 'submit' method is run and the '/potentalSolution' is compared with the actual 'solution' of the 'Cryptogram'. If incorrect, the failed attempts counter increases, and if the 'failedAttempts' attribute is equal to 'maxAttempts' of the 'Cryptogram' then the 'lostGame' of the 'Cryprogram Game' is called and this cryptogram will be recoreded as completed and lost for this player in the database. If the solution was correct, the GUI will show a success message and this cryptogram will be moved to the completed list for this player and recorded in the databse with the date it was solved.. At any point the player can exit the game, which calls the 'exitGame' method of 'CryptogramGame' and this method makes sure to clean up this game session and store the number of 'failedAttempts' (if any) to the database for this specific player and cryptogram.

7. The list of unsolved cryptograms will show the cryptogram’s name and the number of incorrect solution attempts (if any) for that player.

The 'List' class for the GUI can show custom views of the cryptogram called 'UnsolvedCryptogram' which just shows the cryptogram's name and number of failed attempts ('failedAttempts') in this view.

8. The list of completed cryptograms for that player will show the cryptogram’s name, whether it was successfully solved, and the date it was completed.

The 'List' class for the GUI can show custom views of the cryptogram called 'CompletedCryptogram' which just shows the name, whether it was solved or not (based on the 'lost' attribute) and the date it was completed (stored in 'dateOfCompletion' and gathered from the database).

9. The list of cryptogram statistics will display a list of cryptograms in order of creation.  Choosing each cryptogram will display the date it was created, the number of players who have solved the cryptogram, and the username of the first three players to solve the cryptogram.

The 'List' class has a subclass 'CryptogramStatistics' which orders the cryptograms by creationDate and then shows 'CryptogramStatistic' GUI classes that only display the date it was created ('creation'), the number of players who have solved it ('numPlayersSolved') and the first three players who solved it ('firstThreePlayersSolved'). This info is gathered from the database.

10. The User Interface (UI) must be intuitive and responsive.

This is not represented in my design, as it will be handled entirely within the GUI implementation.

11. The performance of the game should be such that players does not experience any considerable lag between their actions and the response of the game.

This is not represented in my design, as it will be handled in a separate design document.
