﻿## Requirements
1. A user will be able to choose to log in as a specific player or create a new player when starting the application.  For simplicity, any authentication is optional, and you may assume there is a single system running the application.
**In the Player class, I added a logIn method to facilitate logging in, and a constructor not shown in the diagram for simplicity will facilitate the creations requirement. For now, authentication is omitted.**
1. The application will allow players to  (1) create a cryptogram, (2) choose a cryptogram to solve, (3) solve cryptograms, (4) view their list of completed cryptograms, and (5) view the list of cryptogram statistics.
**The creation of a cryptogram is taken care of in a constructor not shown. The rest of these methods are implemented in the Player class. Choosing a cryptogram is shown shown by the relationship between the Player and Cryptogram classes called choose. Solving a cryptogram is taken care of in the solveCryptogram method. The viewCompletedCryptograms method will display all Cryptograms of subtype Completed, shown by the dotted line dependency. Finally, viewing the statistics is realized by the viewStatistics method which will order all items in class Cryptogram by attribute createdDate.**
1. A cryptogram will have an encoded phrase (encoded with a simple substitution cipher), a solution, and a maximum number of allowed solution attempts.  A cryptogram will only encode alphabetic characters, but may include other characters (such as punctuation or white space) in the puzzle, which will remain unaltered.  Capitalization is preserved when encoded.
**In the diagram I added a Cryptogram class which has attributes phrase, solution, and maxAttempts, among others. The preservation of capitalization and other characters will be handled in the implementation and is not represented in the design.**
1. To add a player, the user will enter the following player information:
   1. A first name
   1. A last name
   1. A unique username
   1. An email
**In the design there is a class called Player which has four attributes: firstName, lastName, username, and email.**
1. To add a new cryptogram, a player will:
   1. Enter a unique cryptogram puzzle name.
   1. Enter a solution (unencoded) phrase.
   1. Choose different letters to pair with every letter present in the phrase.
   1. View the encoded phrase.
   1. Enter a maximum number of allowed solution attempts.
   1. Edit any of the above steps as necessary.
   1. Save the complete cryptogram.
   1. View a confirmation that the name assigned to the cryptogram is unique and return to the main menu.  
**The act of adding a Cryptogram will be a constructor and therefore is not represented directly in the design. Requirements a, b, d, and e are represented by attributes in the Cryptogram class. Requirement c is represented by a method in the Cryptogram class encodePhrase() that takes the parameter of character pairings and returns the encodedPhrase. Requirement f will be handled in the GUI implementation. Requirement g will be part of the constructor. Finally, Requirement h would be taken care of in a database implementation and the GUI.**
1. To choose and solve a cryptogram, a player will:
   1. Choose a cryptogram from a list of all unsolved cryptograms.
   1. View the chosen cryptogram and number of incorrect solution submissions (starting at 0 for a cryptogram that has not previously been attempted).
   1. Match the replacement and encrypted letters together, and view the resulting potential solution.
   1. When all letters in the puzzle are replaced and they are satisfied with the potential solution, submit their answer.
   1. Get a result indicating that the solution was successful, or incrementing the number of incorrect solution submissions if it was unsuccessful.
   1. At any point, the player may return to the list of unsolved cryptograms to try another.
   1. If the number of incorrect solution attempts increases to the maximum allowed number of solution attempts, they will get a result that the cryptogram game was lost, and this cryptogram will be moved to the completed list.
**Requirement a is taken care of with the chooses relationship and the Unsolved inheritance of Cryptogram. Requirement b will utilize the display method and the attempts attribute of the Unsolved Cryptogram inheritance. Requirement c will be handled within the implementation and utilize the encodePhrase method. Requirement d will also be handled in the implementation, utilizing the encodePhrase method and doing a comparison of the result and solution attribute. The result from Requirement e will be returned from the solveCryptogram method, which will take care of the implementation from the prior requirement. Requirement f will be handled in the GUI. Requirement g will also happen as part of solveCryptogram, comparing attempts of the child class Unsolved to maxAttempts of the parent class Cryptogram. Result will be returned based on that comparison, and the moveToCompleted will shift the Cryptogram from that Player’s Unsolved class to its Completed class.**
1. The list of unsolved cryptograms will show the cryptogram’s name and the number of incorrect solution attempts (if any) for that player.
**This Requirement is taken care of in the dependency relationship between a Player and the Unsolved Cryptogram inheritance, with its attempts attribute.**
1. The list of completed cryptograms for that player will show the cryptogram’s name, whether it was successfully solved, and the date it was completed.
**Similarly to Requirement 7, this Requirement is represented by the dependency relationship between the Player class and the Completed Cryptogram, which has a completeDate and solved attribute. It also inherits the name attribute from its parent.**
1. The list of cryptogram statistics will display a list of cryptograms in order of creation.  Choosing each cryptogram will display the date it was created, the number of players who have solved the cryptogram, and the username of the first three players to solve the cryptogram.
**This requirement is implemented in the viewStatistics method in the Player class. The creationDate attribute will be used to order the list and display the date it was created. The methods getFirstThreeSolvers and numberOfSolvers take care of the other two displays based on the relationship to the Completed Cryptogram inheritance.**
1. The User Interface (UI) must be intuitive and responsive.
**This is not directly represented in the design as it will strictly be handled in the implementation.**
1. The performance of the game should be such that players does not experience any considerable lag between their actions and the response of the game.
**Again, this can’t be represented directly in the design, and will be handled in the implementation.**