﻿# Individual Designs
## Design 1 - Arya
![](img/arya3.png)
### Pros
- Very detailed and shows differences between all subsets of Cryptograms
- Adds in authentication layer
- Good naming conventions, easy to understand the purpose
- Uses utility class

### Cons
- Doesn’t really show a concrete association between the Player and the Cryptogram Statistics/Unsolved/Completed Cryptogram classes
- Missing parameters and return types for operations
- Doesn’t have all the parameters and return types. No bracket after methods.
- Application, List, Cryptogram classes are all separate. I think they should be related to each other.

## Design 2 - Andrew
![](img/akim376.png)
### Pros
- Simple
- Interesting that most of the “work” is done in the helper class
- Good methods for checking username and puzzle name

### Cons
- Getters and Setters don’t need to be listed (not necessarily a bad thing)
- Not sure how it shows in enough details the difference in relationship between a Player and their cryptograms and a Player and the total set of cryptograms. I think it’s there but not completely clear
- Encoded field on Cryptogram can be derived
- No game instance where player is solving
- No login
- Where is info stored about which player has solved what cryptogram?
- ChooseAndSolve Class is doing too much, it’s basically the game itself, can be broken down more.
- Ideally the Cryptogram class has no idea about other classes(like players), this should be extracted into a separate relationship/Class

## Design 3 - Linsey
![](img/lzwiefelhofer3.png)
### Pros
- Very clean and simple
- Good naming conventions. Easy to understand the purpose.
- Has all the parameters and return types to avoid confusion.

### Cons
- Doesn’t have utility class for Date.
- Since UnsolvedCryptogram class has the operation moveToCompleted, I think it should be connected to completedCryptogram class?
- The design suggests a new Cryptogram class (unsolved or completed) is created per player which is wasted memory. How does the Unsolved/Completed Cryptogram know which player it’s associated with?
- No game state
- Login should not be tied to Player class, it’s a separate auth process that then results in a specific Player associated with the username
- Cryptogram class doesn’t need to know about players (e.g. getFirstThreeSolvers)
- viewStatistics() method is not related to the Player Class

# Team Design
![](img/6300GroupClassDiagram.png)
### Commonalities
- Clearly should be a Player Class with
  - Attributes
    - First Name
    - Last Name
    - Username
    - email
- Clearly should be a Cryptogram Class with 
  - Attributes
    - Name
    - Phrase
    - Solution
    - Max Attempts
    - Created Date
  - Methods
    - Get first three solvers
    - Get number of players that have solved
    - statistics gathering function
- Utility class for Date
- Need to define the relationship between the Player and Cryptogram classes
- Cryptogram creation logic that checks the uniqueness of the name is contained in the constructor like in design 3
- Player creation logic that checks the uniqueness of the username
- The cipher is not stored and the phrase and solution are like in design 2 and 3
- Most other logic is in the game app itself, the GUI implementation or in database retrieval methods
### Differences
- Less detail compared to Design 1 as this is only a UML Class diagram and should not have aspects of the system like a database or application state shown
- No List implementation compared to Design 1 since it was deemed unnecessary and overcomplicated the diagram
- Association Class clearly defined between Player and Cryptogram for when a Player chooses a Cryptogram, called ChosenCryptogram
- Association class has only the attributes and methods it needs and is much simpler than in design 2.
- No subclasses of Cryptogram associated with each Player like in design 3, instead the associative class is use to define this relationship.
- Need a User class to be able to login and create a new Player

The final design meets all of the requirements laid out in Assignment 5 and does so without having too much information. Some of the requirements are left to be completed in the implementation itself which helps our UML class diagram be simple and easy to read. With the **User**, **Player** and **Cryptogram** classes along with the associative class **ChosenCryptogram**, the design clearly shows how Users make or login as Players and how Players can iteract with Cryptograms. The use of any external class dependencies is also shown by the inclusion of the **Date** utility class.

We chose not to store the cipher itself in the Cryptogram class and instead simply store the solution and phrase strings. When a Player tries to solve the Cryptogram, the temporary cipher is also not stored anymore, again making our classes simpler. The getter functions are simple and appropriately named and they will basically be queries to the database for lists of Cryptograms or statistics for the different views of our game app.

# Summary
We all had slightly different interpretations of the requirements, which led to the need for a lot more discussion stepping back to the first individual assignment. Instead of pushing for one specific design, we were able to take features from each and combined them.

We all have different technical backgrounds, not only in terms of experience with UML, but even with Object Oriented programming in general, so there was some level-setting and extra explanations that needed to happen.

It was definitely beneficial to have three different perspectives and the ability to talk through them because things from the original requirements that may have been confusing or overlooked made more sense after the conversations.

Another interesting observation was how everybody was given, and took, the opportunity to explain their points of view before moving on or dismissing a topic. That lead to a more amiable working environment.

With these discussions, we were able to agree on the final design and the result is definitely better compared to each of our individual design.
