# Design Document

**Author**: Team 29

## 1 Design Considerations

### 1.1 Assumptions

* Authentication will not be implemented
* Application will be developed and tested with Android Studio

### 1.2 Constraints

The application specfications state that it must be developed for use on an Android device, and must be self-contained within in that device. Similarly, any data must persist within the application, therefore providing implementation limitations in terms of external webservices.

### 1.3 System Environment

The Cryptogram Application is built for an Android Mobile Device and will be deployed on an emulator through Android Studio. The operating system specifications will come after receving further requirements. The application will be entirely self-contained on the device, and as such will only interact with an internal database.

## 2 Architectural Design

### 2.1 Component Diagram

![](img/component-diagram.png)

### 2.2 Deployment Diagram

![](img/deployment-diagram.png)

## 3 Low-Level Design

### 3.1 Class Diagram

![](img/team-design.png)

## 4 User Interface Design

![](img/user-interface.png)

