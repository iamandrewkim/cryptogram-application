# Supplementary requirements

## Introduction

### Purpose
The purpose of this document is to define requirements of the Cryptogram Game. This Supplementary requirements list contains the requirements that are not readily captured in the use cases of the use-case model. The Supplementary requirements and the use-case model together capture a complete set of requirements on the system.

## Requirements

### Usability
The app must be usable and the User Interface (UI) must be intuitive and responsive.

### Performance
The performance of the game should be such that players does not experience any considerable lag between their actions and the response of the game.

### Design Constraints
The app is being built on Android OS. Besides our code and required libraries, no other software or third party is needed for the app to function.

### Persistance
The cryptograms of all players will be saved and accessible by all logged in users. SQLite database will be used for persisting the puzzles and some stats surrounding them.
