# Project Plan - version 1.0.0


**Author**: Team 29

## 1 Introduction

This project is to develop an appication for kids to practice solving cryptograms and compare their progress.

## 2 Process Description


### Software Design
- Description: Our team will create UML design individually. Everyone will analyze each other's design. Then, we discuss using our own designs to produce final design.
- Entrance Criteria: individual UML design
- Exit Criteria: final UML design

### Inception and Elaboration
- Description: Our team will use the final UML design, requirement documents, and templates to create documents that will be basis for completing the product.
- Entrance Criteria: Requirement documentation, final UML design, document templates.
- Exit Criteria: Project plan, use-case model, supplementary requirements, design document, and test plan without results

### Construction
- Description: In this phase, our team will revise earlier documents. We will develop the initial version of the application. We will create test plan with partial results. Finally, we will create user manual for that initial version of the application.
- Entrance Criteria: Documents from previous phase
- Exit Criteria: Revised documents, initial version of the application, test plan with partial results, and user manual

### Transition
- Description: In this phase, we will revise the earlier documents, create test plan with final results, and produce final version of the application
- Entrance Criteria: Documents from previous phase and initial version of the application
- Exit Criteria: Final version of documents, test plan with final results, and final version of the application

## 3 Team

Team Members: Andrew Kim, Linsey Zwiefelhofer, Arya Seghatoleslami
### Roles and Description
- Project Manager: Manages planning, development, and deployment
- Architect: Designs high-level architectural overview of the system
- Development Lead: Leads development planning and execution.
- Developer: Writes codes based on the requirements
- Tester: Implements test cases and scenarios
- UX designer: Designs the application interface for user's ease of use

|Name         | Roles           |
|-------------|:---------------:|
|Andrew       |Project Manager, Tester|
|Linsey       |Architect, UX designer, Developer, Tester|
|Arya         |Development lead, Tester| 

