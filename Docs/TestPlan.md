# Test Plan - version 1.0.0

**Author**: Team 29

## 1 Testing Strategy

### 1.1 Overall strategy

Everyone is our team will write codes for the application. Instead of choosing one tester to write test scripts for all the codes, each one of us will write test script first and then corresponding code after. That will make it easier to track and fix the error later.

### 1.2 Test Selection

We are going to use use black box test since we write test scripts before the actual coding, we will not know what the code looks like. We are going to start with unit test since we have to divide the coding part to each member. One who wrote the code for one unit should also write test scripts for that unit. Then, we are going to use integration testing to make sure the units interact appropriately. Then, we can do functional testing to see if integrated units meet system requirement. Finally, between each version of the application, we would perform regression testing to make sure there is no unexpected changes in performance, inputs and outputs.

### 1.3 Adequacy Criterion

To assess the quality of our test cases, we are going to use functional coverage to observe execution of a test plan.  It will track whether important values, sets of values, or sequences of values that correspond to design or interface requirements, features, or boundary conditions have been exercised. By fulfilling the functional coverage, we can be confident in our quality of our test cases because we can test all the required functionalities. 

### 1.4 Bug Tracking

When users find bugs and make enhancement requests, they will go to our database for developers to review.

### 1.5 Technology

We are going to use JUnit 4 to test our codes

## 2 Test Cases


|Purpose   | Steps necessary to perform the test | Expected Result | Actual Result | Pass/Fail | Additional Info|
|----------------|:-----------------------------------:|:---------------:|:----------:|:-----:|:------:|
|Add a player| enter attributes: firstName, lastName, username, and email| Player added| Player added | Pass | |
|Add a cryptogram| From choose view, click create new button. Then, enter attributes: puzzle name, solution phrase, cipher, max attempts, complete *matchLettersForPhrase* function | A message is displayed saying the puzzle is created. Then, view goes back to *Choose view* with the created puzzle name and the number of attempts performed(0) |  A message is displayed saying the puzzle is created. Then, view goes back to *Choose view* with the created puzzle name and the number of attempts performed(0) | Pass | |
|Add a player with the duplicate username| enter existing username | Sign in as that username instead of register| Sign in as that username instead of register |Pass | Since we have no authentication function, if user enters any existing username, he will enter as that player. If the username is not in the database, then the user will create a new player with that username. |
|Add a cryptogram with the duplicate puzzle name| enter existing puzzle name| Message printed saying the puzzle name is not unique| Message printed saying the puzzle name is already existing | Pass | The message is displayed in the bottom using toast |
|Choose a cryptogram|From *Choose* view, click the the cryptogram| The view will be converted to *Solve* view| The view is converted to *Solve view*|Pass||
|Submitting the correct phrase| Submit the correct answer before the max attempt is reached | Message printed saying the puzzle is solved. The view will be converted to *View* view. |Message printed saying the puzzle is solved. The view is converted to *View view*. |Pass| |
|Submitting the incorrect phrase| Submit the incorrect answer | Message printed saying the phrase is incorrect. The view stays the same|Message printed saying the phrase is incorrect. The view stays the same |Pass | |
|See if maximum attempt works well| Submitting the incorrect phrase after it reaches the maximum attempt allowed | Message is printed saying the phrase is incorrect. The view is converted to *View view*| Message is printed saying the phrase is incorrect. The view is converted to *View view* |Pass | |
|View the list of all the puzzles| Click *View* view and click the radio button *View Statistics*| All the created puzzles are shown| All the created puzzles are shown|Pass||
|View the list of all the completed puzzles| Click *View* view and click the radio button *View Completed*| Only the completed puzzles are shown| Only the completed puzzles are shown|Pass||
|Check the attributes of the puzzle in the list| From *View* view, click the radio button *View Statistics*. Then, click each puzzle.| Toast message pops up with puzzle name, created date, number of players solved, and first three solvers|Toast message pops up with puzzle name, created date, number of players solved, and first three solvers| Pass ||
|Check the attributes of the puzzle in the completed list| From *View* view, click the radio button *View Completed*. Then, click each puzzle.| Toast message pops up with puzzle name, Solved/ Failed, Date completed|Toast message pops up with puzzle name, Solved/ Failed, Date completed| Pass ||
|See if the different player can choose the puzzles created by other player| Create a new player and go to *Choose* view| All the puzzles are shown| All the puzzles are shown| Pass| |
|See if the different player can see the puzzles created by other player from the list| Create a new player and go to *View* view. Then, click the radio button *View Statistics*| All the puzzles are shown| All the puzzles are shown| Pass| |
|See if completed list is empty for new player|Create a new player and go to *View* view. Then, click the radio button *View Completed*.| The list is empty| The list is empty| Pass||
|See if the first three solvers will be in order| Create a new player and click the puzzle that is already solved by other player. Solve the puzzle successfully. Then, go to *View* view. Click the *View Statistics* radio button. Then, click the solved puzzle.| From the message, the first three solvers has the usernames in order.| From the message, the first three solvers has the usernames in order.| Pass||
|See if the number of players solved gets updated| Create a new player. Click the puzzle that is already solved by other player. Solve the puzzle successfully. Then, go to *View* view. Click the *View Statistics* radio button. Then, click the solved puzzle.|From the message, the number of players solved is increased by 1| From the message, the number of players solved is increased by 1|Pass||
|See the 4th solver would change the first three solvers| Create a new player. Click the puzzle that is already solved by three other players. Solve the puzzle successfully. Then, go to *View* view. Click the *View Statistics* radio button. Then, click the solved puzzle. | From the message, the first three solvers are not changed. The 4th solver's username is not on it.| From the message, the first three solvers are not changed. The 4th solver's username is not on it.| Pass||