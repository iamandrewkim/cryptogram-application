# Use Case Model

**Author**: Team29

## 1 Use Case Diagram

![](img/use-case-diagram.png)

## 2 Use Case Descriptions

### Login
#### Requirements
The User should be able to login as a Player to the app using a username.

#### Pre-conditions
The User is not logged in.

#### Post-conditions
The User is logged in.

#### Scenarios
**Basic Flow**
1. User types in username
2. User clicks on "login" button
3. User is logged in and can access the app

**Alternative Flows**
- User's username is not registered

	If the username does not have a Player associated with it, an unknown username error is shown.

### Create a Player
#### Requirements
The User should be able to create a Player with a unique username to then login as that Player to access the app.

#### Pre-conditions
The User is not logged in.

#### Post-conditions
A Player is created that is associated with the given username and the User is logged in.

#### Scenarios
**Basic Flow**
1. User clicks on "create player" button
2. User enters in first name, last name, email, and a unique username
3. User clicks on "create" button
4. User is logged in as new player

**Alternative Flows**
- User enters in a username that is already in use

	If the username is not unique, an error is shown that the username is already taken.
- User doesn't enter anything in the username field

	If the username field is empty, an error is thrown for not providing any username
- User doesn't provide first name, last name, and/or email

	If the User doesn't provide info but provides a unique username, he is still signed in and the Player is valid.

### See list of unsolved cryptograms
#### Requirements
The User can see a list of unsolved cryptogram to choose from to solve once logged in.

#### Pre-conditions
User is logged in.

#### Post-conditions
None.

#### Scenarios
**Basic Flow**
1. User logs in as Player X
2. User sees a list of unsolved cryptogram for Player X

**Alternative Flows**
- Player has solved all cryptograms

	If Player X has solved all the current cryptograms than a congratulatory message is shown.

### Choose to solve a cryptogram
#### Requirements
The User can choose a cryptogram from the list of unsolved cryptograms to try to solve it.

#### Pre-conditions
User is logged in and has unsolved cryptograms.

#### Post-conditions
None.

#### Scenarios
**Basic Flow**
1. User logs in
2. User is presented with a list of unsolved cryptograms
3. User chooses an unsolved cryptogram
4. User is taken to the solving screen for a cryptogram

**Alternative Flows**

None

### Attempt to solve a cryptogram
#### Requirements
The Player can attempt to solve an unsolved cryptogram up to the number of max attempts of the cryptogram.

#### Pre-conditions
User is logged in and has unsolved cryptogram and has chosen an unsolved cryptogram.

#### Post-conditions
Cryptogram is either solved and moved to completed cryptogram, or unsolved and another attempt is possible unless max attempts is reached. If max attempt is reached, the cryptogram is moved to completed cryptograms as unsolved.

#### Scenarios
**Basic Flow**
1. User logs in
2. User chooses an unsolved cryptogram
3. User matches up letters for the cryptogram phrase
4. User submits his/her solution
5. User is presented with a "success" message

**Alternative Flows**
- User gets the solution wrong and still has attempts
	
	If the User gets the solution wrong, he/she is presented with a "failure" message and is encouraged to try again

- User is wrong and has reach max attempts

	If the User gets the solution wrong and has tried as many times as allowed, he/she is presented with a "failure" message and a "no more attempts" message.

### Create a Cryptogram
#### Requirements
A Player can create a cryptogram.

#### Pre-conditions
User is logged in.

#### Post-conditions
New Cryptogram is made and available to solve for other Players.

#### Scenarios
**Basic Flow**
1. User logs in
2. User clicks "Create a cryptogram" button
3. User enters in a puzzle name
4. User enters in a solution phrase
5. User chooses different letters to pair with every letter in the phrase
6. User views the encoded phrase
7. User enters in max number of attempts
8. User saves the cryptogram
9. User sees a confirmation that the name assigned to the cryptogram is unique and return to the main menu.

**Alternative Flows**
- User enters a non-unique name

	If the User enters a non-unique name for the cryptogram then, when he/she clicks the save button, he/she is presented with an error message explaining that the entered name is already in use.

- User does not pair any letters with the solution phrase

	If the User does not create a cipher by not pairing any letters with the solution phrase, he/she is prompted with a message asking them to pair at least one letter with another.

### See list of completed cryptograms
#### Requirements
A Player can see the list of cryptograms they completed, whether it was solved or unsolved, and the date it was completed.

#### Pre-conditions
User is logged.

#### Post-conditions
None.

#### Scenarios
**Basic Flow**
1. User logs in
2. User sees the list view of cryptograms
3. User chooses the completed cryptogram list view

**Alternative Flows**
- Player has no completed cryptograms
	
	If the Player has no completed cryptograms, then the list view will be empty.

### See list of cryptograms statistics
#### Requirements
A Player can see a list of cryptogram statistics in order of creation.

#### Pre-conditions
User is logged.

#### Post-conditions
None.

#### Scenarios
**Basic Flow**
1. User logs in
2. User sees the list view of cryptograms
3. User chooses the cryptogram statistics list view

**Alternative Flows**
- No cryptograms are created

	If no cryptograms are created then the view will be empty.

### Choose to see a cryptogram statistic
#### Requirements
A Player can choose a cryptogram from the cryptogram statistics list view to see it's statistics.

#### Pre-conditions
User is logged in and there are cryptograms.

#### Post-conditions
Cryptogram statistics are loaded for viewing.

#### Scenarios
**Basic Flow**
1. User logs in
2. User sees the list view of cryptograms
3. User chooses the cryptogram statistics list view
4. User chooses a specific cryptogram
5. User sees the statistics for the chosen cryptogram

**Alternative Flows**

None
