# User Manual - version 1.0.0

## Introduction
SDPCryptogram is an app for kids that lets them practice solving cryptograms and compare their progress with their friends. Anyone with an android device can download and use it.

## Manual
### Start the app
Click on the app in your phone to start and open up the SDPCryptogram app.

### enter in a username
If you haven't used the app before, go ahead and enter a username you want to use when playing the game. If someone else already has the username, you'll have to try another one.

If you already have a username, go ahead and enter that and sign in to your old progress saved!

### Choose a Cryptogram to solve
The first thing you'll see is a list of cryptograms that you haven't solved. Touch any one to begin solving it!

### Solve Cryptogram
Select the letters that should replace the letters in the phrase. Click *VIEW DECODED* button to see the resulting phrase. If you are satisfied with the result, click *SUBMIT* button. If it is correct, it will show the message saying "Solved". If not, it will show the message saying "Wrong Solution"

### Create a Cryptogram
Click on the "Create New" button to create a cryptogram! Enter in a name, your secret phrase, and then select which letters should replace the letters in the phrase, creating your cryptogram. Clicking view puzzle shows you your secret encoded phrase. Finally, enter in the number of tries you want to give other players to solve your puzzle!

### View Cryptogram Statistics
Touch the View tab towards the top of the screen to see the list of all cryptograms with some statistics about them each. You can see when it was made, the number of players that have solved it, and the first three players that solved it!

### View Your Completed Cryptograms
Now touch the "View Completed" radio button to see a list of cryptograms you've completed. The list will tell you which cryptogram you've successfully solved and the date you completed it.
