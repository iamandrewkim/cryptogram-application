package edu.gatech.seclass.sdpcryptogram;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChooseFragment extends Fragment {
    private View rootView;
    private String mUsername;

    public ChooseFragment() {
        // Required empty public constructor
    }

    private View.OnClickListener selectPuzzle = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String puzzleName = v.getTag().toString();
            GlobalVariables.currentPuzzle = puzzleName;
            TabLayout host = getActivity().findViewById(R.id.tabs);

            host.getTabAt(1).select();
            EventBus.getDefault().post(new DataEvent(puzzleName));
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_choose, container, false);
        mUsername = getArguments().getString("username");
        final List<TextView> allTvs = new ArrayList<TextView>();

        LinearLayout unsolvedCryptograms = rootView.findViewById(R.id.llCryptograms);
        CryptogramDBHelper dbHelper = new CryptogramDBHelper(getContext());
        //Returns cryptogram id
        Cursor cursor = dbHelper.getUnsolvedPuzzlesByPlayer(mUsername);

        while (cursor.moveToNext()) {
            int cryptoId = cursor.getInt(cursor.getColumnIndex("cryptogram_id"));
            Cursor pCursor = dbHelper.getPuzzleById(cryptoId);
            pCursor.moveToFirst();
            TextView tv = new TextView(getContext());
            String content = "";
            String puzzleName = pCursor.getString(pCursor.getColumnIndex("name"));

            content += puzzleName;
            content += "     " + cursor.getString(cursor.getColumnIndex("attempts"));
            LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            allTvs.add(tv);
            tv.setLayoutParams(lParams);
            tv.setText(content);
            tv.setTag(puzzleName);
            tv.setOnClickListener(selectPuzzle);
            unsolvedCryptograms.addView(tv);
            pCursor.close();
        }
        cursor.close();

        Button createNewCryptogramButton = rootView.findViewById(R.id.create_new_cryptogram_button);
        createNewCryptogramButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CreateCryptogramActivity.class);
                getActivity().startActivity(intent);
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        LinearLayout unsolvedCryptograms = rootView.findViewById(R.id.llCryptograms);
        CryptogramDBHelper dbHelper = new CryptogramDBHelper(getContext());
        //Returns cryptogram id
        Cursor cursor = dbHelper.getUnsolvedPuzzlesByPlayer(mUsername);

        unsolvedCryptograms.removeAllViews();
        while (cursor.moveToNext()) {
            int cryptoId = cursor.getInt(cursor.getColumnIndex("cryptogram_id"));
            Cursor pCursor = dbHelper.getPuzzleById(cryptoId);
            pCursor.moveToFirst();
            TextView tv = new TextView(getContext());
            String content = "";
            String puzzleName = pCursor.getString(pCursor.getColumnIndex("name"));

            content += puzzleName;
            content += "     " + cursor.getString(cursor.getColumnIndex("attempts"));
            LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            tv.setLayoutParams(lParams);
            tv.setText(content);
            tv.setTag(puzzleName);
            tv.setOnClickListener(selectPuzzle);
            unsolvedCryptograms.addView(tv);
            pCursor.close();
        }
        cursor.close();
    }
}
