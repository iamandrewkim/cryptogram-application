package edu.gatech.seclass.sdpcryptogram;

public class CompleteEvent {
    private final String message;

    public CompleteEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
