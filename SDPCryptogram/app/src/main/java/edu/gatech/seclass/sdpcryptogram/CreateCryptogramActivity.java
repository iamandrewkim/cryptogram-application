package edu.gatech.seclass.sdpcryptogram;

import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.ArrayMap;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class CreateCryptogramActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_cryptogram);

        final CryptogramDBHelper dbHelper = new CryptogramDBHelper(this);

        Button submitButton = findViewById(R.id.submit_new_cryptogram_button);
        Button cipherButton = findViewById(R.id.create_cipher_button);
        Button viewPuzzleButton = findViewById(R.id.view_puzzle_button);
        final EditText name = findViewById(R.id.etName);
        final EditText phrase = findViewById(R.id.etPhrase);
        final LinearLayout ll = findViewById(R.id.llCipher);
        final List<TextView> allTvs = new ArrayList<TextView>();
        final List<EditText> allEds = new ArrayList<EditText>();
        final TextView tvEncodedPuzzle = findViewById(R.id.tvEncodedPuzzle);
        final ArrayMap<String, String> letterMapping = new ArrayMap<>();
        final Set<Character> phraseChars = new LinkedHashSet<>();
        final EditText maxAttempts = findViewById(R.id.etMaxAttempts);

        cipherButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                char[] phraseText = phrase.getText().toString().toCharArray();
                for(int i=0; i<phraseText.length; i++) {
                    phraseChars.add(phraseText[i]);
                }
                Iterator<Character> itr = phraseChars.iterator();
                while(itr.hasNext()) {
                    LinearLayout verticalView = new LinearLayout(getApplicationContext());
                    verticalView.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    EditText et = new EditText(getApplicationContext());
                    TextView tv = new TextView(getApplicationContext());
                    et.setBackgroundTintList(ColorStateList.valueOf(Color.GRAY));
                    et.setTextColor(Color.GRAY);
                    tv.setTextColor(Color.GRAY);
                    allTvs.add(tv);
                    allEds.add(et);
                    et.setLayoutParams(lparams);
                    tv.setLayoutParams(lparams);
                    verticalView.setLayoutParams(lparams);
                    tv.setText(String.valueOf(itr.next()));
                    verticalView.addView(et);
                    verticalView.addView(tv);
                    ll.addView(verticalView);
                }
            }
        });

        viewPuzzleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String encodedPuzzle = "";
                for(int i=0; i<allEds.size(); i++) {
                    letterMapping.put(allTvs.get(i).getText().toString(),allEds.get(i).getText().toString());
                }
                char[] phraseText = phrase.getText().toString().toCharArray();
                for(int i=0; i<phraseText.length;i++) {
                    encodedPuzzle += letterMapping.get(String.valueOf(phraseText[i]));
                }
                tvEncodedPuzzle.setText(encodedPuzzle);
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO: validate inputs

                Cursor cursor = dbHelper.getPuzzleByName(name.getText().toString());
                if(cursor.getCount() == 0) {
                    //save to db
                    if(dbHelper.insertPuzzle(
                            name.getText().toString(),
                            phrase.getText().toString(),
                            tvEncodedPuzzle.getText().toString(),
                            Integer.parseInt(maxAttempts.getText().toString())
                    )) {
                        //get all players and insert into their lists
                        Cursor playerCursor = dbHelper.getAllPlayers();
                        while (playerCursor.moveToNext()) {
                            if (dbHelper.insertChosenPuzzle(
                                    name.getText().toString(),
                                    playerCursor.getString(playerCursor.getColumnIndex("username"))
                            )) {
                                Toast.makeText(getApplicationContext(), "Chosen puzzle successfully created!", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Toast.makeText(getApplicationContext(), "Chosen puzzle couldn't be created", Toast.LENGTH_SHORT).show();
                            }
                        }
                        playerCursor.close();
                        finish();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Puzzle couldn't be created", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    //name already exists
                    Toast.makeText(getApplicationContext(),
                            "Puzzle with name " + name.getText().toString() + " already exists", Toast.LENGTH_SHORT).show();
                    name.requestFocus();
                }
                cursor.close();
            }
        });
    }
}
