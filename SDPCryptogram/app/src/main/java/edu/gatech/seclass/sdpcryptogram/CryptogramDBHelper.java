package edu.gatech.seclass.sdpcryptogram;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CryptogramDBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "CryptogramHelper.db";
    private static final int DATABASE_VERSION = 1;

    //Cryptogram Table
    public static final String CRYPTOGRAM_TABLE_NAME = "cryptogram";
    public static final String CRYPTOGRAM_COLUMN_ID = "_id";
    public static final String CRYPTOGRAM_COLUMN_NAME = "name";
    public static final String CRYPTOGRAM_COLUMN_PHRASE = "phrase";
    public static final String CRYPTOGRAM_COLUMN_SOLUTION = "solution";
    public static final String CRYPTOGRAM_COLUMN_MAX_ATTEMPTS = "max_attempts";
    public static final String CRYPTOGRAM_COLUMN_CREATED_DATE = "created_date";

    //Player Table
    public static final String PLAYER_TABLE_NAME = "player";
    public static final String PLAYER_COLUMN_ID = "_id";
    public static final String PLAYER_COLUMN_NAME = "name";
    public static final String PLAYER_COLUMN_USERNAME = "username";
    public static final String PLAYER_COLUMN_EMAIL = "email";

    //ChosenCryptogram Table
    public static final String CHOSEN_CRYPTOGRAM_TABLE_NAME = "chosen_cryptogram";
    public static final String CHOSEN_CRYPTOGRAM_COLUMN_ID = "_id";
    public static final String CHOSEN_CRYPTOGRAM_COLUMN_DATE_COMPLETED = "date_completed";
    public static final String CHOSEN_CRYPTOGRAM_COLUMN_COMPLETED = "completed";
    public static final String CHOSEN_CRYPTOGRAM_COLUMN_SOLVED = "solved";
    public static final String CHOSEN_CRYPTOGRAM_COLUMN_ATTEMPTS = "attempts";
    public static final String COLUMN_FOREIGN_KEY_CRYPTOGRAM = "cryptogram_id";
    public static final String COLUMN_FOREIGN_KEY_PLAYER = "player_id";


    public CryptogramDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + CRYPTOGRAM_TABLE_NAME + "(" +
                        CRYPTOGRAM_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        CRYPTOGRAM_COLUMN_NAME + " TEXT, " +
                        CRYPTOGRAM_COLUMN_PHRASE + " TEXT, " +
                        CRYPTOGRAM_COLUMN_SOLUTION + " TEXT, " +
                        CRYPTOGRAM_COLUMN_MAX_ATTEMPTS + " INTEGER, " +
                        CRYPTOGRAM_COLUMN_CREATED_DATE + " TEXT" + ")"
        );

        db.execSQL(
                "CREATE TABLE " + PLAYER_TABLE_NAME + "(" +
                        PLAYER_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        PLAYER_COLUMN_NAME + " TEXT, " +
                        PLAYER_COLUMN_USERNAME + " TEXT, " +
                        PLAYER_COLUMN_EMAIL + " TEXT" + ")"
        );

        db.execSQL(
                "CREATE TABLE " + CHOSEN_CRYPTOGRAM_TABLE_NAME + "(" +
                        CHOSEN_CRYPTOGRAM_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        CHOSEN_CRYPTOGRAM_COLUMN_DATE_COMPLETED + " TEXT, " +
                        CHOSEN_CRYPTOGRAM_COLUMN_COMPLETED + " INTEGER DEFAULT 0 NOT NULL, " +
                        CHOSEN_CRYPTOGRAM_COLUMN_SOLVED + " INTEGER DEFAULT 0 NOT NULL, " +
                        CHOSEN_CRYPTOGRAM_COLUMN_ATTEMPTS + " INTEGER DEFAULT 0 NOT NULL, " +
                        COLUMN_FOREIGN_KEY_CRYPTOGRAM + " INTEGER NOT NULL, " +
                        COLUMN_FOREIGN_KEY_PLAYER + " INTEGER NOT NULL, " +
                        "FOREIGN KEY(" + COLUMN_FOREIGN_KEY_CRYPTOGRAM + ") REFERENCES cryptogram(_id), " +
                        "FOREIGN KEY(" + COLUMN_FOREIGN_KEY_PLAYER + ") REFERENCES player(_id))"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CRYPTOGRAM_TABLE_NAME);
        onCreate(db);
    }

    public boolean insertPlayer(String firstName, String lastName, String username, String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(PLAYER_COLUMN_NAME, firstName + " " + lastName);
        contentValues.put(PLAYER_COLUMN_EMAIL, email);
        contentValues.put(PLAYER_COLUMN_USERNAME, username);

        int playerID = (int) db.insert(PLAYER_TABLE_NAME, null, contentValues);

        Cursor cursor = getAllPuzzles();
        contentValues = new ContentValues();

        while (cursor.moveToNext()) {
            int puzzleID = cursor.getInt(cursor.getColumnIndex("_id"));

            contentValues.put(COLUMN_FOREIGN_KEY_CRYPTOGRAM, puzzleID);
            contentValues.put(COLUMN_FOREIGN_KEY_PLAYER, playerID);
            contentValues.put(CHOSEN_CRYPTOGRAM_COLUMN_ATTEMPTS, 0);
            contentValues.put(CHOSEN_CRYPTOGRAM_COLUMN_COMPLETED, 0);
            contentValues.put(CHOSEN_CRYPTOGRAM_COLUMN_SOLVED, 0);

            db.insert(CHOSEN_CRYPTOGRAM_TABLE_NAME, null, contentValues);
        }
        cursor.close();

        return true;
    }

    public Cursor getPlayer(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        return  db.rawQuery("SELECT * FROM " + PLAYER_TABLE_NAME + " WHERE " +
                PLAYER_COLUMN_USERNAME + "=?", new String[]{username});
    }

    public int getPlayerID(String username) {
        Cursor c = getPlayer(username);

        c.moveToFirst();
        int playerID = c.getInt(c.getColumnIndex("_id"));
        c.close();

        return playerID;
    }

    public Cursor getAllPlayers() {
        SQLiteDatabase db = this.getReadableDatabase();
        return  db.rawQuery("SELECT * FROM " + PLAYER_TABLE_NAME, null);
    }

    public boolean insertPuzzle(String name, String solution, String phrase, Integer maxAttempts) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(CRYPTOGRAM_COLUMN_NAME, name);
        contentValues.put(CRYPTOGRAM_COLUMN_SOLUTION, solution);
        contentValues.put(CRYPTOGRAM_COLUMN_PHRASE, phrase);
        contentValues.put(CRYPTOGRAM_COLUMN_MAX_ATTEMPTS, maxAttempts);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(new Date());
        contentValues.put(CRYPTOGRAM_COLUMN_CREATED_DATE, date);

        db.insert(CRYPTOGRAM_TABLE_NAME, null, contentValues);
        return true;
    }

    public Cursor getPuzzleByName(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        return  db.rawQuery("SELECT * FROM " + CRYPTOGRAM_TABLE_NAME + " WHERE " +
                CRYPTOGRAM_COLUMN_NAME + "=?", new String[]{name});
    }

    public int getPuzzleID(String name) {
        Cursor c = getPuzzleByName(name);

        c.moveToFirst();
        int puzzleID = c.getInt(c.getColumnIndex("_id"));
        c.close();

        return puzzleID;
    }

    public int getPuzzleMaxAttempts(String name) {
        Cursor c = getPuzzleByName(name);

        c.moveToFirst();
        int maxAttempts = c.getInt(c.getColumnIndex("max_attempts"));
        c.close();

        return maxAttempts;
    }

    public Cursor getAllPuzzles() {
        SQLiteDatabase db = this.getReadableDatabase();
        return  db.rawQuery("SELECT * FROM " + CRYPTOGRAM_TABLE_NAME, null);
    }

    public Cursor getPuzzleById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        return  db.rawQuery("SELECT * FROM " + CRYPTOGRAM_TABLE_NAME + " WHERE " +
                CRYPTOGRAM_COLUMN_ID + "=?", new String[]{String.valueOf(id)});
    }

    public boolean insertChosenPuzzle(String puzzleName, String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        Cursor cursor = getPlayer(username);
        cursor.moveToFirst();
        int playerID = Integer.parseInt(cursor.getString(cursor.getColumnIndex("_id")));
        cursor = getPuzzleByName(puzzleName);
        cursor.moveToFirst();
        int puzzleID = Integer.parseInt(cursor.getString(cursor.getColumnIndex("_id")));
        cursor.close();

        contentValues.put(COLUMN_FOREIGN_KEY_CRYPTOGRAM, puzzleID);
        contentValues.put(COLUMN_FOREIGN_KEY_PLAYER, playerID);
        contentValues.put(CHOSEN_CRYPTOGRAM_COLUMN_ATTEMPTS, 0);
        contentValues.put(CHOSEN_CRYPTOGRAM_COLUMN_COMPLETED, 0);
        contentValues.put(CHOSEN_CRYPTOGRAM_COLUMN_SOLVED, 0);

        db.insert(CHOSEN_CRYPTOGRAM_TABLE_NAME, null, contentValues);
        return true;
    }

    public Cursor getChosenPuzzleById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        return  db.rawQuery("SELECT * FROM " + CHOSEN_CRYPTOGRAM_TABLE_NAME + " WHERE " +
                CHOSEN_CRYPTOGRAM_COLUMN_ID + "=?", new String[]{String.valueOf(id)});
    }

    public Cursor getChosenPuzzleByIds(int puzzleID, int playerID) {
        SQLiteDatabase db = this.getReadableDatabase();
        return  db.rawQuery("SELECT * FROM " + CHOSEN_CRYPTOGRAM_TABLE_NAME + " WHERE " +
                COLUMN_FOREIGN_KEY_CRYPTOGRAM + "=?" + " AND " + COLUMN_FOREIGN_KEY_PLAYER + "=?",
                new String[]{String.valueOf(puzzleID), String.valueOf(playerID)});
    }

    public boolean updateSolvedChosenPuzzle(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(new Date());
        ContentValues contentValues = new ContentValues();

        Cursor c = getChosenPuzzleById(id);
        c.moveToFirst();
        int attempts = Integer.parseInt(c.getString(c.getColumnIndex("attempts")));
        c.close();

        contentValues.put(CHOSEN_CRYPTOGRAM_COLUMN_ATTEMPTS, attempts + 1);
        contentValues.put(CHOSEN_CRYPTOGRAM_COLUMN_COMPLETED, 1);
        contentValues.put(CHOSEN_CRYPTOGRAM_COLUMN_SOLVED, 1);
        contentValues.put(CHOSEN_CRYPTOGRAM_COLUMN_DATE_COMPLETED, date);

        db.update(CHOSEN_CRYPTOGRAM_TABLE_NAME, contentValues, "_id=?",
                new String[]{String.valueOf(id)});
        return true;
    }

    public boolean updateCompletedPuzzle(int chosenPuzzleID) {
        SQLiteDatabase db = this.getWritableDatabase();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(new Date());
        ContentValues contentValues = new ContentValues();

        contentValues.put(CHOSEN_CRYPTOGRAM_COLUMN_COMPLETED, 1);
        contentValues.put(CHOSEN_CRYPTOGRAM_COLUMN_DATE_COMPLETED, date);

        db.update(CHOSEN_CRYPTOGRAM_TABLE_NAME, contentValues, "_id=?",
                new String[]{String.valueOf(chosenPuzzleID)});
        return true;
    }

    public Cursor getUnsolvedPuzzlesByPlayer(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = getPlayer(username);
        cursor.moveToFirst();
        String playerID = cursor.getString(cursor.getColumnIndex("_id"));
        cursor.close();

        return  db.rawQuery("SELECT * FROM " + CHOSEN_CRYPTOGRAM_TABLE_NAME + " WHERE " +
                COLUMN_FOREIGN_KEY_PLAYER + "=?" + " AND " + CHOSEN_CRYPTOGRAM_COLUMN_COMPLETED +
                "=?", new String[]{playerID, "0"});
    }

    public Cursor getPuzzleSolvers(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        return  db.rawQuery("SELECT * FROM " + CHOSEN_CRYPTOGRAM_TABLE_NAME + " WHERE " +
                COLUMN_FOREIGN_KEY_CRYPTOGRAM + "=?" + " AND " + CHOSEN_CRYPTOGRAM_COLUMN_SOLVED +
                "=?", new String[]{String.valueOf(id), "1"});
    }

    public List<String> getFirstThreeSolvers(Cursor cursor) {
        List<String> usernames = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        int count = 0;

        while(cursor.moveToNext() && count < 3) {
            String playerID = cursor.getString(cursor.getColumnIndex("player_id"));
            Cursor res =  db.rawQuery("SELECT * FROM " + PLAYER_TABLE_NAME + " WHERE " +
                    PLAYER_COLUMN_ID + "=?", new String[]{playerID});

            res.moveToFirst();
            usernames.add(res.getString(res.getColumnIndex("username")));
            res.close();
            count++;
        }

        return usernames;
    }

    public Cursor getCompletedPuzzlesByPlayer(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        int playerID = getPlayerID(username);

        return db.rawQuery("SELECT * FROM " + CHOSEN_CRYPTOGRAM_TABLE_NAME + " INNER JOIN " +
                CRYPTOGRAM_TABLE_NAME + " ON " + CHOSEN_CRYPTOGRAM_TABLE_NAME + ".cryptogram_id = " +
                CRYPTOGRAM_TABLE_NAME + "._id" + " WHERE " + COLUMN_FOREIGN_KEY_PLAYER + "=?" +
                " AND " + CHOSEN_CRYPTOGRAM_COLUMN_COMPLETED + "=?",
                new String[]{String.valueOf(playerID), "1"});
    }

    public boolean updatePuzzleAttempts(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        Cursor c = getChosenPuzzleById(id);
        c.moveToFirst();
        int attempts = c.getInt(c.getColumnIndex("attempts"));
        c.close();

        contentValues.put(CHOSEN_CRYPTOGRAM_COLUMN_ATTEMPTS, attempts + 1);

        db.update(CHOSEN_CRYPTOGRAM_TABLE_NAME, contentValues, "_id=?",
                new String[]{String.valueOf(id)});
        return true;
    }
}
