package edu.gatech.seclass.sdpcryptogram;

public class DataEvent {
    private final String message;

    public DataEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
