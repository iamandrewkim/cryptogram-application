package edu.gatech.seclass.sdpcryptogram;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        TextView usernameTextView = findViewById(R.id.register_username);
        final EditText emailEditText = findViewById(R.id.register_email);
        final EditText firstNameEditText = findViewById(R.id.register_first_name);
        final EditText lastNameEditText = findViewById(R.id.register_last_name);
        Button registerButton = findViewById(R.id.register_button);
        final CryptogramDBHelper dbHelper = new CryptogramDBHelper(this);

        usernameTextView.setText(getIntent().getStringExtra("username"));
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dbHelper.insertPlayer(firstNameEditText.getText().toString(), lastNameEditText.getText().toString(),
                        getIntent().getStringExtra("username"), emailEditText.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Player Inserted", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra("username", getIntent().getStringExtra("username"));
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getApplicationContext(), "Failed to insert player", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}
