package edu.gatech.seclass.sdpcryptogram;

import android.content.Context;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 */
public class SolveFragment extends Fragment {
    private CryptogramDBHelper dbHelper;
    private View rootView;

    public SolveFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_solve, container, false);
        EventBus.getDefault().register(this);

        return rootView;
    }

    @Subscribe
    public void onMessageEvent(DataEvent event){
        GlobalVariables.currentPuzzle = event.getMessage();
        final Context context = rootView.getContext().getApplicationContext();
        final String mUsername = getArguments().getString("username");
        CryptogramDBHelper dbHelper = new CryptogramDBHelper(getContext());
        final int puzzleID = dbHelper.getPuzzleID(GlobalVariables.currentPuzzle);
        final int playerID = dbHelper.getPlayerID(mUsername);
        final TextView encodedPuzzle = getActivity().findViewById(R.id.tvEncoded);
        final Button submitButton = getActivity().findViewById(R.id.submit_solution_Button);
        Cursor pCursor = dbHelper.getPuzzleByName(GlobalVariables.currentPuzzle);

        pCursor.moveToFirst();
        final String phrase = pCursor.getString(pCursor.getColumnIndex("phrase"));
        encodedPuzzle.setText(phrase);

        final String solution = pCursor.getString(pCursor.getColumnIndex("solution"));
        Set<Character> phraseChars = new LinkedHashSet<>();
        final List<TextView> allTvs = new ArrayList<>();
        final List<EditText> allEds = new ArrayList<>();
        final LinearLayout ll = getActivity().findViewById(R.id.llDecodeCipher);
        final ArrayMap<String, String> letterMapping = new ArrayMap<>();

        char[] phraseText = phrase.toCharArray();
        for (int i=0; i < phraseText.length; i++) {
            phraseChars.add(phraseText[i]);
        }
        ll.removeAllViews();
        Iterator<Character> itr = phraseChars.iterator();
        while (itr.hasNext()) {
            LinearLayout verticalView = new LinearLayout(getContext());
            LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            EditText et = new EditText(getContext());
            TextView tv = new TextView(getContext());

            verticalView.setOrientation(LinearLayout.VERTICAL);
            et.setBackgroundTintList(ColorStateList.valueOf(Color.GRAY));
            et.setTextColor(Color.GRAY);
            tv.setTextColor(Color.GRAY);
            allTvs.add(tv);
            allEds.add(et);
            et.setLayoutParams(lparams);
            tv.setLayoutParams(lparams);
            verticalView.setLayoutParams(lparams);
            tv.setText(String.valueOf(itr.next()));
            verticalView.addView(et);
            verticalView.addView(tv);
            ll.addView(verticalView);
        }

        Button viewResultButton = getActivity().findViewById(R.id.btnViewDecoded);
        viewResultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView decoded = getActivity().findViewById(R.id.tvDecoded);
                String encodedPuzzle = "";
                char[] phraseText = phrase.toCharArray();

                for (int i=0; i < allEds.size(); i++) {
                    letterMapping.put(allTvs.get(i).getText().toString(),allEds.get(i).getText().toString());
                }
                for (int i=0; i < phraseText.length; i++) {
                    encodedPuzzle += letterMapping.get(String.valueOf(phraseText[i]));
                }

                decoded.setText(encodedPuzzle);
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean result;
                TextView decoded = getActivity().findViewById(R.id.tvDecoded);
                String stringDecoded = decoded.getText().toString();
                CryptogramDBHelper dbHelper = new CryptogramDBHelper(getContext());

                Cursor chosenPuzzleCursor = dbHelper.getChosenPuzzleByIds(puzzleID, playerID);
                chosenPuzzleCursor.moveToFirst();
                final int chosenPuzzleID = chosenPuzzleCursor.getInt(chosenPuzzleCursor.getColumnIndex("_id"));

                //increase attempts
                dbHelper.updatePuzzleAttempts(chosenPuzzleID);
                chosenPuzzleCursor = dbHelper.getChosenPuzzleById(chosenPuzzleID);
                chosenPuzzleCursor.moveToFirst();

                if (stringDecoded.equals(solution)) {
                    result = dbHelper.updateSolvedChosenPuzzle(chosenPuzzleID);
                    if (result) {
                        encodedPuzzle.setText("");
                        decoded.setText("");
                        allEds.clear();
                        allTvs.clear();
                        ll.removeAllViews();
                        TabLayout host = getActivity().findViewById(R.id.tabs);
                        host.getTabAt(2).select();
                        Toast.makeText(context, "Solved!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Error! Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(context, "Wrong solution", Toast.LENGTH_SHORT).show();
                    Integer maxAttempts = dbHelper.getPuzzleMaxAttempts(GlobalVariables.currentPuzzle);

                    //check if attempts equals max attempts
                    int attempts = chosenPuzzleCursor.getInt(chosenPuzzleCursor.getColumnIndex("attempts"));
                    if (attempts >= maxAttempts) {
                        result = dbHelper.updateCompletedPuzzle(chosenPuzzleID);
                        if (result) {
                            encodedPuzzle.setText("");
                            decoded.setText("");
                            allEds.clear();
                            allTvs.clear();
                            ll.removeAllViews();
                            TabLayout host = getActivity().findViewById(R.id.tabs);
                            host.getTabAt(2).select();
                        }
                    }
                }
                chosenPuzzleCursor.close();
            }
        });
        pCursor.close();
    }
}
