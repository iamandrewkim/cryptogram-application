package edu.gatech.seclass.sdpcryptogram;


import android.app.AlertDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.List;

import static android.database.DatabaseUtils.dumpCursorToString;


/**
 * A simple {@link Fragment} subclass.
 */
public class ViewFragment extends Fragment {
    private View rootView;
    private CryptogramDBHelper dbHelper;
    private LinearLayout cryptograms;
    private String mUsername;

    public ViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_view, container, false);
        dbHelper = new CryptogramDBHelper(getContext());
        cryptograms = rootView.findViewById(R.id.llList);
        mUsername = getArguments().getString("username");
        final RadioGroup radioGroup = rootView.findViewById(R.id.rgView);

        renderSolveData(mUsername);

        radioGroup.setOnCheckedChangeListener(new
          RadioGroup.OnCheckedChangeListener() {
              @Override
              public void onCheckedChanged(RadioGroup group, int checkedId) {
                  View radioButton = radioGroup.findViewById(checkedId);
                  int index = radioGroup.indexOfChild(radioButton);

                  cryptograms.removeAllViews();
                  switch (index) {
                      case 0: // User chose completed
                          //set list to completed content
                          renderSolveData(mUsername);
                          break;
                      case 1: // User chose statistics
                          //set list to statistics content
                          renderCryptogramStatistics();
                          break;
                      default: // No choice made.
                          // Do nothing.
                          break;
                  }
              }
          });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        cryptograms.removeAllViews();
        renderSolveData(mUsername);
    }

    private void renderCryptogramStatistics() {
        Cursor allPuzzlesCursor = dbHelper.getAllPuzzles();

        while (allPuzzlesCursor.moveToNext()) {
            TextView tv = new TextView(getContext());
            String content = "";
            String puzzleName = allPuzzlesCursor.getString(allPuzzlesCursor.getColumnIndex("name"));
            content += puzzleName;
            LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            final int puzzleID = allPuzzlesCursor.getInt(allPuzzlesCursor.getColumnIndex("_id"));

            tv.setLayoutParams(lparams);
            tv.setText(content);
            tv.setTag(puzzleName);
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Cursor puzzleCursor = dbHelper.getPuzzleById(puzzleID);
                    puzzleCursor.moveToFirst();
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String puzzleName = v.getTag().toString();

                    Cursor solveCountCursor = dbHelper.getPuzzleSolvers(puzzleID);
                    String info = puzzleName + "\n" +
                            "created on: " +
                            puzzleCursor.getString(puzzleCursor.getColumnIndex("created_date")) +
                            "\n" + "Number of players solved: " + solveCountCursor.getCount() +
                            "\n" + "First three solvers: ";

                    List<String> usernames = dbHelper.getFirstThreeSolvers(solveCountCursor);
                    while(!usernames.isEmpty()) {
                        info += usernames.remove(0) + " ";
                    }

                    builder.setMessage(info);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    solveCountCursor.close();
                    puzzleCursor.close();
                }
            });
            cryptograms.addView(tv);
        }
        allPuzzlesCursor.close();
    }

    private void renderSolveData(String username) {
        Cursor cursor = dbHelper.getCompletedPuzzlesByPlayer(username);

        while(cursor.moveToNext()) {
            TextView tv = new TextView(getContext());
            String content = "";
            String puzzleName = cursor.getString(cursor.getColumnIndex("name"));
            content += puzzleName;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            final int completedPuzzleID = cursor.getInt(cursor.getColumnIndex("_id"));

            tv.setLayoutParams(layoutParams);
            tv.setText(content);
            tv.setTag(puzzleName);
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String puzzleName = v.getTag().toString();
                    Cursor infoCursor = dbHelper.getChosenPuzzleById(completedPuzzleID);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String message;

                    infoCursor.moveToFirst();
                    if (infoCursor.getInt(infoCursor.getColumnIndex("solved")) == 1) {
                        message = "Solved";
                    }
                    else {
                        message = "Failed";
                    }
                    String info = puzzleName + "\n" + message + "\nDate completed: " +
                            infoCursor.getString(infoCursor.getColumnIndex("date_completed")) +
                            "\n";

                    infoCursor.close();
                    builder.setMessage(info);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });
            cryptograms.addView(tv);
        }
        cursor.close();
    }
}
